trigger ClosedOpportunityTrigger on Opportunity (after insert, before update, after update) {

    if(trigger.isBefore){
        if(trigger.isUpdate){
        
        }
    }
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            OpportunityHandler.afterInsert(trigger.new,trigger.oldMap);
        }
        if(trigger.isUpdate){
            OpportunityHandler.afterUpdate(trigger.new,trigger.oldMap);
        }
    }

}