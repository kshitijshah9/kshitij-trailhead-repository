trigger LeaveTriger on Leave__c (after insert, after update) {
    
    if(trigger.isInsert && trigger.isAfter){
        LeaveTriggerHandler.afterinsert(trigger.new);
    }
}