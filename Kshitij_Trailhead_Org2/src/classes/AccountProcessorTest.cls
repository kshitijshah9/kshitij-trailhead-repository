@isTest
private class AccountProcessorTest{
    
    @isTest static void testcountContacts() {
        
        Test.startTest();
        
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Id> accIds = new List<Id>();
        
        for(Integer i=1;i<=10;i++){
            Account acc = new Account(Name = 'Test Account'+i);
            accList.add(acc);
        }
        insert accList;
        
        for(Account accRec : accList){
            accIds.add(accRec.Id);
        }
        
        for(Integer i=1;i<=8;i++){
            Contact con = new Contact(LastName = 'Test LName'+i, FirstName = 'Test Fname'+i,AccountId = accList[i].Id);
            conList.add(con);
        }
        insert conList;

        AccountProcessor.countContacts(accIds);
        Test.stopTest();
    }
    
    
}