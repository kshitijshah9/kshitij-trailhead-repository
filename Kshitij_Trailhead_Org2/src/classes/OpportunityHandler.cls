public class OpportunityHandler{

    public static void afterInsert(list<Opportunity> newOppList, map<Id,Opportunity> oldOppMap){
        OpportunityHelper.createTaskforWonOpp(newOppList, oldOppMap);
    }
    
    public static void afterUpdate(list<Opportunity> newOppList, map<Id,Opportunity> oldOppMap){
        OpportunityHelper.createTaskforWonOpp(newOppList, oldOppMap);
    }
    
    
    
}