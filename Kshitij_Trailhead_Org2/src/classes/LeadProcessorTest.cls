@isTest
private class LeadProcessorTest{

    @isTest static void testLeadSourceBatchUpdate() {
        
        
        
        List<Lead> leadList = new List<Lead>();
        
        for(Integer i=0; i<200; i++){
            Lead leadRec = new Lead(Lastname = 'Test Lastname'+i, Company = 'Jade');
            leadList.add(leadRec);
        }
        
        insert leadList;
        
        Test.startTest();
        
        LeadProcessor leadProcessorBatch = new LeadProcessor();
        Database.executeBatch(leadProcessorBatch);
        
        Test.stopTest();
    }
}