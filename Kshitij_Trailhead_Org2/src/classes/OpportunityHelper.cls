public class OpportunityHelper{

    public static void createTaskforWonOpp(list<Opportunity> newOppList, map<Id,Opportunity> oldOppMap){
        
        set<Id> oppIds = new set<Id>();
        
        for(Opportunity oppRec : newOppList){
            if(oppRec.StageName == 'Closed Won'){
                oppIds.add(oppRec.Id);
            }   
        }
        
        if(oppIds != null && oppIds.size() > 0){
            
            list<Task> taskList = new list<Task>();
            
            for(Id opptyId : oppIds){
                Task taskRec = new Task(Subject = 'Follow Up Test Task',WhatId=opptyId,Status = 'Open',Priority = 'Normal');
                taskList.add(taskRec);
            }
            
            insert taskList;
        }
        
    }
    
}