global class DailyLeadProcessor implements Schedulable{
    
    global void execute(SchedulableContext ctx) {
        
        List<Lead> leadList = new List<Lead>();
        leadList = [select id,LeadSource from Lead where LeadSource = null limit 200];
        
        for(Lead leadRec : leadList){
            leadRec.LeadSource = 'Dreamforce';
        }
        
        update leadList;
        
    }
    
}