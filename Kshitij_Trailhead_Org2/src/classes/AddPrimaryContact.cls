public class AddPrimaryContact implements Queueable{
    
    private Contact contactRec;
    private String stateAbbr;
    
    public AddPrimaryContact(Contact conRec, String state) {
        this.contactRec = conRec;
        this.stateAbbr = state;
    }

    public void execute(QueueableContext context) {
        List<Account> accList = new List<Account>();
        List<Contact> contactList = new List<Contact>();
        accList = [select id,BillingState from Account where BillingState =: stateAbbr limit 200];
        for (Account account : accList) {
          Contact con = contactRec.clone(false, false, false, false);
          con.AccountId=account.Id;
          contactList.add(con);
        }
        insert contactList;
    }
    
}