global class ABC{

    public abstract class A{
        public String str1 = 'A Class';
    }
    
    public abstract class B extends A{
       public String str2;
    }
    public class C extends B{
       public String str3;
       C obj = new C();
       public void mthd1(){
           system.debug(obj.str1);
       }
       
       public ABC.C method1(){
           system.debug('In Method 1');
           String request = 'Test Request';
           Map<String,String> response = new Map<String,String>();
           response.put('Test Request','Test Response');
           List<String> strLst = new List<String>();
           strLst.add('Test String');
           //WebServiceCallout.invoke(this,request,response,strLst);
           return new C();
       }
       
    }
    
    webservice static String getStringMethod(){
        return 'Test String';
    }
    
    public abstract class D {
       public String str4;
       
    }
    
    public class E extends D{
       public C strC;
       public D strD;
       
       
    }
}