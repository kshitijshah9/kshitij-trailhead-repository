@isTest
private class AddPrimaryContactTest{
    
    @isTest static void testcountContacts() {
        
        
        
        List<Account> accList = new List<Account>();
        
        for(Integer i=1;i<=50;i++){
            Account acc1 = new Account(Name = 'Test Account NY'+i,BillingState = 'NY');
            accList.add(acc1);
            Account acc2 = new Account(Name = 'Test Account CA'+i,BillingState = 'CA');
            accList.add(acc2);
        }
        insert accList;
        
        Contact con = new Contact(Lastname = 'Test Lastname');
        
        AddPrimaryContact updater = new AddPrimaryContact(con, 'CA');
        
        Test.startTest();
        System.enqueueJob(updater);
        Test.stopTest();
        
        System.assertEquals(50, [select count() from contact where Account.BillingState = 'CA']);
    }
    
    
}