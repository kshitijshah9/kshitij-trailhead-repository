public class RandomContactFactory{
    
    public static List<Contact> generateRandomContacts(Integer numContacts, String conLastName) {
        
        list<Contact> conList = new List<Contact>();
        
        for(Integer i=1;i<=numContacts;i++){
            Contact con = new Contact(LastName = conLastName, FirstName = 'Test Fname'+i);
            conList.add(con);
        }
        
        return conList;
    }

}