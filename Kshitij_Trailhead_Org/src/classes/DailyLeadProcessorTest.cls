@isTest
private class DailyLeadProcessorTest{
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    @isTest static void testLeadSourceBatchUpdate() {
        List<Lead> leadList = new List<Lead>();
        
        for(Integer i=0; i<200; i++){
            Lead leadRec = new Lead(Lastname = 'Test Lastname'+i, Company = 'Jade');
            leadList.add(leadRec);
        }
        
        insert leadList;
        
        Test.startTest();
        
        String jobId = System.schedule('ScheduledApexTest',CRON_EXP,new DailyLeadProcessor()); 
        
        Test.stopTest();
    
    }
    
}