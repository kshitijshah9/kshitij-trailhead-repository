public class AccountProcessor{
    
    @future
    public static void countContacts(List<Id> accIds){
        
        list<Account> accList = new List<Account>();
        
        if(accIds != null && accIds.size() > 0){
            accList = [select id,Number_of_Contacts__c,(select id from Contacts) from Account where id in: accIds];
            
            if(accList != null && accList.size() > 0){
                for(Account accRec : accList){
                    accRec.Number_of_Contacts__c = accRec.Contacts.size();
                }
                update accList;
            }
            
        }
        
    }
    

}