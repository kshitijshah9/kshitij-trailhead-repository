@isTest
private class TestVerifyDate{
    @isTest static void testCheckDates1() {
        Date d1 = Date.newInstance(2017, 7, 7);
        Date d2 = Date.newInstance(2017, 7, 27);
        VerifyDate.CheckDates(d1,d2);
    }
    
    @isTest static void testCheckDates2() {
        Date d1 = Date.newInstance(2017, 7, 7);
        Date d2 = Date.newInstance(2017, 3, 27);
        VerifyDate.CheckDates(d1,d2);
    }
    
}