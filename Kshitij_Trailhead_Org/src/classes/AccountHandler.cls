public class AccountHandler{

    public static void beforeInsert(list<Account> newAccList, map<Id,Account> oldAccMap){
        AccountHelper.updateShippingCode(newAccList, oldAccMap);
    }
    
    public static void beforeUpdate(list<Account> newAccList, map<Id,Account> oldAccMap){
        AccountHelper.updateShippingCode(newAccList, oldAccMap);
    }
    
    
    
}