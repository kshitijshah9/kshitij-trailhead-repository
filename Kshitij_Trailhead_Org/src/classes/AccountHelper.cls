public class AccountHelper{

    public static void updateShippingCode(list<Account> newAccList, map<Id,Account> oldAccMap){
        
        for(Account acc : newAccList){
            if(acc.BillingPostalCode != null && acc.Match_Billing_Address__c){
                acc.ShippingPostalCode = acc.BillingPostalCode;
            }
        }
    }
    
}