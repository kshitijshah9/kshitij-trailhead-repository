@isTest
private class TestRestrictContactByName{

    @isTest static void testRestrictContactByNameInsertMethod() {
        Contact con = new Contact(LastName = 'INVALIDNAME',FirstName = 'Test');
        Test.startTest();
        Database.insert(con,false);
        Test.stopTest();

    }
    
    @isTest static void testRestrictContactByNameUpdateMethod() {
        Contact con = new Contact(LastName = 'LASTNAME',FirstName = 'Test');
        insert con;
        con.LastName = 'INVALIDNAME';
        Test.startTest();
        Database.update(con,false);
        Test.stopTest();
    }
    
}