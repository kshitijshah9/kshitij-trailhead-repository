public class LeaveTriggerHandler{

    public static void afterinsert(list<Leave__c> newList){
        LeaveTriggerHelper.updateLeaveCount(newList);
    }
    
}