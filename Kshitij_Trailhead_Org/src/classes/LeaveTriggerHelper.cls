public class LeaveTriggerHelper{

    public static void updateLeaveCount(list<Leave__c> newList){
        
        set<Id> contIds = new set<Id>();
        list<Contact> conList = new list<Contact>();
        
        for(Leave__c leaveObj : newList){
            if(leaveObj.Employee__c != null){
                contIds.add(leaveObj.Employee__c);
            }
        }
        
        if(contIds != null && contIds.size() > 0){
            conList = [select id,Number_of_Leaves__c,(select id from Leaves__r) from Contact where id in: contIds];
            
            if(conList != null && conList.size() > 0){
                for(Contact con : conList){
                    for(Leave__c leaveRec : con.Leaves__r){
                        con.Number_of_Leaves__c = con.Leaves__r.size();
                    }
                }
                
                update conList;
            
            }
        }
    }
    
}