trigger AccountAddressTrigger on Account (before insert, before update) {
    
    if(trigger.isBefore){
        if(trigger.isInsert){
            AccountHandler.beforeInsert(trigger.new,trigger.oldMap);
        }
        if(trigger.isUpdate){
            AccountHandler.beforeUpdate(trigger.new,trigger.oldMap);
        }
    }
    
    if(trigger.isAfter){
        
    }
    
}